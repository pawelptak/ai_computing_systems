from mpi4py import MPI
import numpy as np
import time

if __name__ == '__main__':
    comm = MPI.COMM_WORLD
    rank = comm.rank
    size = comm.size
    msg_buffer_size = 4

    if rank == 0:
        data = np.zeros(msg_buffer_size * size).reshape(size, msg_buffer_size)
        requests = []  # new!!

        # create data to be sent
        for i in range(1, size):
            data[i, :] = np.arange(i, i + msg_buffer_size, 1)
            print(data)
            print(f"{rank} has initialized sync transfer", flush=True)
            request = comm.Isend(data[i, :], dest=i)  # asynchronous send
            requests.append(request)

        for req in requests:  # wait for requests to be completed
            req.wait()

        print(f"{rank} all transfers completed.", flush=True)
    else:
        time.sleep(rank)
        data = np.zeros(msg_buffer_size)  # need to create variable for recieved message
        request = comm.Irecv(data, source=0)  # asynchronous receive
        request.wait()  # important!
        print(f"{rank} has received {data}", flush=True)

#  synchronic - comm.SSend, comm.Recv
