from mpi4py import MPI
import numpy as np

if __name__ == '__main__':
    comm = MPI.COMM_WORLD
    rank = comm.rank
    size = comm.size
    msg_buffer_size = 4

    if rank == 0:
        data = np.zeros(msg_buffer_size * size).reshape(size, msg_buffer_size)

        # create data to be sent
        for i in range(1, size):
            data[i, :] = np.arange(i, i + msg_buffer_size, 1)
            comm.Send(data[i, :], dest=i)
            print(f"{rank} has sent {data[i, :]} to {i}", flush=True)
    else:
        data = np.zeros(msg_buffer_size)  # need to create variable for recieved message
        comm.Recv(data, source=0)
        print(f"{rank} has received {data}", flush=True)

#  synchronic - comm.SSend, comm.Recv
