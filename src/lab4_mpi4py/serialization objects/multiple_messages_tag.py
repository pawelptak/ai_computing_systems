from mpi4py import MPI
import time

if __name__ == '__main__':
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()

    if rank == 0:
        destination_rank = 4
        for i in range(5):
            data = f"Text number {i}"
            print(f"{rank} is about to send {data} with tag {i}", flush=True)
            comm.send(data, dest=destination_rank, tag=i)


    if rank == 4:
        source_rank = 0
        for i in range(4, -1, -1):  # reverse order
            print(f"{rank} is about to receive tag {i}", flush=True)
            recv_data = comm.recv(source=source_rank, tag=i)
            print(f"{rank} has received '{recv_data}' from {source_rank}", flush=True)
