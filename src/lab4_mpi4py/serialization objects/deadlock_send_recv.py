from mpi4py import MPI
import time

if __name__ == '__main__':
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()

    if rank == 1:
        data = "A"*10**3
        destination_rank = 2
        source_rank = 2

        received_data = comm.sendrecv(data, source=source_rank, dest=destination_rank)  #sending and receiving
        print(f"{rank} Finished", flush=True)

    if rank == 2:
        data = "B" * 10 ** 3
        destination_rank = 1
        source_rank = 1
        received_data = comm.sendrecv(data, source=source_rank, dest=destination_rank)
        print(f"{rank} has received '{received_data}' from {source_rank}", flush=True)
