from mpi4py import MPI
import time

if __name__ == '__main__':
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()

    print(f"My rank is: {rank}", flush=True)

    if rank == 0:
        data = "Some text from rank 0"
        destination_rank = 4
        print(f"{rank} is about to send")
        comm.send(data, dest=destination_rank)  # sends data to buffer, can be blocked if data not received
        print(f"{rank} has sent '{data}' to {destination_rank}", flush=True)

    if rank == 4:
        source_rank = 0
        print(f"{rank} is about to receive")
        time.sleep(10)
        recv_data = comm.recv(source=source_rank)
        print(f"{rank} has received '{recv_data}' from {source_rank}", flush=True)
