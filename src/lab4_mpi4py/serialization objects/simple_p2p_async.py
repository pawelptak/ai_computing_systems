from mpi4py import MPI
import time

if __name__ == '__main__':
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    print(f"My rank is: {rank}", flush=True)

    if rank == 0:
        for i in range(1, size):
            data = f"Text number {i}"
            print(f"{rank} before async send of msg number {i}", flush=True)
            request = comm.isend(data, dest=i)  # asynchronous send, wont block program run
            print(f"{rank} waiting for the message {i} to be sent", flush=True)
            request.wait()
            print(f"{rank} has sent the message number {i}", flush=True)
    else:
        print(f"{rank} before async receive", flush=True)
        request = comm.irecv(source=0)  # asynchronous receive
        print(f"{rank} is waiting for message", flush=True)
        data = request.wait()
        print(f"{rank} has received '{data}'", flush=True)

