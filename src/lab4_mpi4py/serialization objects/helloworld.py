from mpi4py import MPI
import os

if __name__ == '__main__':

    # Initializing communicator
    comm = MPI.COMM_WORLD

    # Ranks are processes IDs
    rank = comm.Get_rank()  # Rank of process who calls this function
    size = comm.Get_size()  # Total number of processes in communicator

    if rank == 0:
        version = MPI.Get_version()
        hostName = MPI.Get_processor_name()
        print(f"MPI version {version}; Processor name {hostName}")
    # All processess run the same code
    print(f"Hello world from process: {rank}/{size}, PID {os.getpid()}", flush=True)
