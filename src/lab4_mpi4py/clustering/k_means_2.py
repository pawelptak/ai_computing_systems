import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import os
from sklearn.cluster import KMeans

df = pd.read_csv('Mall_Customers.csv')
df.rename(columns={'Annual Income (k$)' : 'Income', 'Spending Score (1-100)' : 'Spending_Score'}, inplace = True)

sns.pairplot(df[['Age','Income', 'Spending_Score']])
plt.show()

kmeans = KMeans(n_clusters=5).fit(df[['Spending_Score','Income']])
df['Clusters'] = kmeans.labels_
print(df.head())

sns.scatterplot(x="Spending_Score", y="Income",hue = 'Clusters',  data=df)
plt.show()
