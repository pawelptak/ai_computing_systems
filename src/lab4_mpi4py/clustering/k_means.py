# Types of clustering:
# - Hard clustering - ostre. Each element belongs to exactly one cluster. E.g. K-Means Clustering
# - Overlapping clustering - rozmyte. Each element belongs to multiple clusters.
# - Hierarchical clustering.
#
# K-Means:
# 1. Select K number
# 2. Randomly select 3 data points (centroids)
# 3. Measure distance (e.g. Euclidean) between the 1st point of data and the 3 selected points (clusters).
# 4. Assign 1st point to the nearest cluster
# 5. Calculate the mean values for each cluster - these are the new centroids
# 6. Measure distance between the 2nd point of data and the 3 centroids
# 7. Mean
# 8. Repeat until centroids don't change
#
# Finding K:
# 1. multiple tries and verify the variation in each cluster
# 2. put on plot - x: number of clusters, y: reduction in variance
# 3. Find elbow on plot - the point when the variation stops reducing quickly

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.DataFrame({
    'x': [12, 20, 28, 18, 29, 33, 24, 45, 52, 51, 52, 55, 53, 55, 61, 64, 69, 72],
    'y': [39, 36, 30, 52, 46, 55, 59, 63, 70, 66, 63, 58, 23, 14, 8, 19, 7, 24]
})

from sklearn.cluster import KMeans

kmeans = KMeans(n_clusters=3)
kmeans.fit(df)
labels = kmeans.predict(df)
centroids = kmeans.cluster_centers_

fig = plt.figure(figsize=(5, 5))
plt.scatter(df['x'], df['y'], color='k', edgecolor='k')
for idx, centroid in enumerate(centroids):
    plt.scatter(*centroid, color=['r'])
plt.xlim(0, 80)
plt.ylim(0, 80)
plt.show()