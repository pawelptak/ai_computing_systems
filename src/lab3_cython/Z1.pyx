import numpy as  np

def convolve_cython(image: double[:,:], kernel: long[:,:]) -> float[:,:]:
    kernel_x: cython.int = kernel.shape[0]
    kernel_y: cython.int = kernel.shape[1]
    img_x: cython.int = image.shape[0]
    img_y: cython.int = image.shape[1]


    output_x: cython.int = int((img_x + - kernel_x) / 1 + 1)
    output_y: cython.int = int((img_y + - kernel_y) / 1 + 1)

    output: cython.np.ndarray = np.zeros([output_x, output_y], dtype=np.float64)

    a: cython.int = img_x
    b: cython.int = img_y

    img_padded = image

    for y in range(img_y):
        # check if kernel is at end of image
        if y > img_y - kernel_y:
            break
        for x in range(img_x):
            # check if kernel is at end of image
            if x > img_x - kernel_x:
                break
            try:
                output[x, y] = np.sum((kernel * img_padded[x: x + kernel_x, y: y + kernel_y]))
            except:
                break
    return output
