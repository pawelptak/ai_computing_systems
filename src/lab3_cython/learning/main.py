import numpy as np
import time
from conv_cython import convolve_cython, convolve_cython_multiproc
from conv2d import convolve, convolve_multiproc
import cv2


if __name__ == '__main__':

    img = cv2.imread('lena.png',0)
    img = np.pad(img, 1)
    img = np.float64((img > 128) * 255)

    kernel = np.array([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]])


    start = time.time()
    out = convolve(img, kernel)
    end = time.time()
    result = end - start
    print(f'Time {result}')
    cv2.imwrite('img_1.png', out)

    start = time.time()
    out = convolve_cython(img, kernel)
    end = time.time()
    result = end - start
    print(f'Time {result}')
    cv2.imwrite('img.png', out)

