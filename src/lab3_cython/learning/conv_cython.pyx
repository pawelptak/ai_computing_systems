#DOEST WORK -- OUTPUTS BLACK IMAGE

import numpy as np
import multiprocessing

def convolve_cython(image: double[:,:], kernel: long[:,:]) -> float[:,:]:
    kernel_x: cython.int = kernel.shape[0]
    kernel_y: cython.int = kernel.shape[1]
    img_x: cython.int = image.shape[0]
    img_y: cython.int = image.shape[1]


    output_x: cython.int = int((img_x + - kernel_x) / 1 + 1)
    output_y: cython.int = int((img_y + - kernel_y) / 1 + 1)

    output: cython.np.ndarray = np.zeros([output_x, output_y], dtype=np.float64)

    img_padded = image

    for y in range(img_y):
        # check if kernel is at end of image
        if y > img_y - kernel_y:
            break
        for x in range(img_x):
            # check if kernel is at end of image
            if x > img_x - kernel_x:
                break
            try:
                output[x, y] = np.sum((kernel * img_padded[x: x + kernel_x, y: y + kernel_y]))
            except:
                break
    return output



def get_output(args):
    img_x: cython.int = args[0]
    img_y: cython.int = args[1]
    kernel: np.ndarray = args[2]
    img_padded: np.ndarray = args[3]
    y: cython.int = args[4]
    kernel_x: cython.int = kernel.shape[0]
    kernel_y: cython.int = kernel.shape[1]
    output = []
    if y > img_y - kernel_y:
        return
    for x in range(img_padded.shape[0]):
        if x > img_padded.shape[0] - kernel_x:
            break
        output.append((kernel * img_padded[x:x + kernel_x, y: y + kernel_y]).sum())
    return output

def convolve_cython_multiproc(image: np.ndarray, kernel: np.ndarray, padding: int):
    kernel_x: cython.int = kernel.shape[0]
    kernel_y: cython.int = kernel.shape[1]
    img_x: cython.int = image.shape[0]
    img_y: cython.int = image.shape[1]

    output_x = int((img_x + 2 * padding - kernel_x) / 1 + 1)
    output_y = int((img_y + 2 * padding - kernel_y) / 1 + 1)

    output: cython.np.ndarray = np.zeros([output_x, output_y], dtype=image.dtype)

    a: cython.int = img_x + padding * 2
    b: cython.int = img_y + padding * 2
    if padding:
        img_padded: cython.np.ndarray = np.zeros([a, b], dtype=image.dtype)

        # adding actual image inside padded image
        img_padded[int(padding):int(-1 * padding), int(padding):int(-1 * padding)] = image
    else:
        img_padded = image

    args = [[img_x, img_y, kernel, img_padded, y] for y in range(img_y)]

    with multiprocessing.Pool(4) as pool:
        results = pool.map(get_output, args)
    for i in range(len(results)):
        if results[i]:
            output[:, i] = results[i]
    return output

