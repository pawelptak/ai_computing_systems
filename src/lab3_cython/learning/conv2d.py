import numpy as np
import multiprocessing

def convolve(image, kernel):
    kernel_x = kernel.shape[0]
    kernel_y = kernel.shape[1]
    img_x = image.shape[0]
    img_y = image.shape[1]

    output_x = int((img_x - kernel_x) / 1 + 1)
    output_y = int((img_y - kernel_y) / 1 + 1)

    output = np.zeros((output_x, output_y))
    img_padded = image

    for y in range(img_y):
        # check if kernel is at end of image
        if y > img_y - kernel_y:
            break
        for x in range(img_x):
            # check if kernel is at end of image
            if x > img_x - kernel_x:
                break
            try:
                output[x, y] = np.sum((kernel * img_padded[x: x + kernel_x, y: y + kernel_y]))
            except:
                break
    return output



def get_output(args):
    img_x, img_y, kernel, img_padded, y = args
    kernel_x = kernel.shape[0]
    kernel_y = kernel.shape[1]
    output = []
    if y > img_y - kernel_y:
        return
    for x in range(img_padded.shape[0]):
        if x > img_padded.shape[0] - kernel_x:
            break
        output.append((kernel * img_padded[x:x + kernel_x, y: y + kernel_y]).sum())
    return output


def convolve_multiproc(image, kernel, padding=0):
    kernel_x = kernel.shape[0]
    kernel_y = kernel.shape[1]
    img_x = image.shape[0]
    img_y = image.shape[1]

    output_x = int((img_x + 2 * padding - kernel_x) / 1 + 1)
    output_y = int((img_y + 2 * padding - kernel_y) / 1 + 1)

    output = np.zeros((output_x, output_y))

    if padding:
        img_padded = np.zeros((img_x + padding * 2, img_y + padding * 2))

        # adding actual image inside padded image
        img_padded[int(padding):int(-1 * padding), int(padding):int(-1 * padding)] = image
    else:
        img_padded = image

    args = [[img_x, img_y, kernel, img_padded, v] for v in range(img_y)]

    with multiprocessing.Pool(4) as pool:
        results = pool.map(get_output, args)
    for i in range(len(results)):
        if results[i]:
            output[:, i] = results[i]
    return output

