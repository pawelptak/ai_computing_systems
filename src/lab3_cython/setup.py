from setuptools import setup
from Cython.Build import cythonize

setup(
    ext_modules=cythonize("Z1.pyx"),
    zip_safe=False,
)

# build command:
# python setup.py build_ext --inplace