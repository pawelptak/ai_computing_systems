import numpy as np
import cv2
from Z1 import convolve_cython
import time

kernel = np.array([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]])
img = cv2.imread('lena.png', 0)
img = np.float64((img > 128) * 255)
img = np.pad(img, 1)

# start = time.time()
# convolve(image=img, kernel=kernel, padding=1)
# end = time.time()
# result = end-start
# print(f'convolve time: {result}')
# print(out_img.shape)
# cv2.imwrite('out.png', out_img)


start = time.time()
img = convolve_cython(image=img, kernel=kernel)
end = time.time()
result = end-start
print(f'convolve time: {result}')
# print(out_img.shape)
cv2.imwrite('cython_out.png', img)
