# zad 1
import requests
import io
import numpy as np
from matplotlib import pyplot as plt
import time

base_url = "http://156.17.43.89:8080/sysoai/"

response = requests.get(base_url)
json = response.json()

all_data = []

matrix = np.zeros((1024, 1024))

start = time.time()

index = 0
for file in json:
    if file['type'] == 'file':
        url = '%s%s' % (
            base_url, file['name']
        )
        response = requests.get(url)
        response.raise_for_status()
        data = np.load(io.BytesIO(response.content))
        x, y = index//16, index % 16
        index += 1
        matrix[x*64:(x+1)*64, y*64:(y+1)*64] = data
end = time.time()
result = end-start
print(result)


plt.imshow(matrix)
plt.savefig("image.png")
