# zad 3
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
import os
import threading
import time


clfs = {
    'kNN': KNeighborsClassifier(),
    'CART': DecisionTreeClassifier(random_state=1234),
}

datasets_long = os.listdir("./datasets")
datasets = []
for dataset in datasets_long:
    datasets.append(dataset[:-4])

def train(X, y, data_id):
    for fold_id, (train, test) in enumerate(rskf.split(X, y)):
        for clf_id, clf_name in enumerate(clfs):
            clf = clone(clfs[clf_name])
            clf.fit(X[train], y[train])
            y_pred = clf.predict(X[test])
            scores[clf_id, data_id, fold_id] = accuracy_score(y[test], y_pred)

start = time.time()
threads = list()

import numpy as np
from sklearn.model_selection import RepeatedStratifiedKFold

n_datasets = len(datasets)
n_splits = 5
n_repeats = 2
rskf = RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats, random_state=1234)

scores = np.zeros((len(clfs), n_datasets, n_splits * n_repeats))


from sklearn.base import clone
from sklearn.metrics import accuracy_score

for data_id, dataset in enumerate(datasets):
    dataset = np.genfromtxt("datasets/%s.csv" % (dataset), delimiter=",")
    X = dataset[:, :-1]
    y = dataset[:, -1].astype(int)
    thread = threading.Thread(target=train, args=(X, y, data_id))
    thread.start()
    threads.append(thread)



np.save('results', scores)
end = time.time()
result_time = end-start

print("Time: ", result_time)

for thr in threads:
    thr.join()

scores = np.load('results.npy')
print("\nScores:\n", scores.shape)
